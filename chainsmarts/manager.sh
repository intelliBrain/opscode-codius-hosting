#!/usr/local/bin/env bash

# This script prompts the user for needed information to deploy in AWS.

## Setting up a dev environment.

# - Setup an [XRP testnet account](https://ripple.com/build/xrp-test-net/).
# - Codius testnet?
# - Configure environment variables (secret key for the testnet, aws information, etc...).
# - Configure certbot with the appropiate timeouts for dns challenge.

## Setting up a production environment.

# - Where to store you keys? (SSM?)
# - What about a domain name?

echo "This script isn't implemented yet. More to come..."
echo "Stay current by following the master branch at https://gitlab.com/jfgrissom/opscode-codius-hosting/"

# Related to infrastructure:
# Create a subscription to the CentOS ami image in the Amazon Marketplace. This makes the image that the CentOs team maintains available to the region you are hosting in.
# Setup VPC
# -- Subnets
# -- Route Tables
# -- Internet Gateway
# -- Security Groups