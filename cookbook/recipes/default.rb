#
# Cookbook:: codius-hosting
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

# Set the hostname codius.yourdomain.tld
# TODO: Handle the ability to define multiple hostnames for multiple nodes.
# Examples:
#   codius01.yourdomain.tld, codius02.yourtomain.tld 
#   and
#   codius01.dev.yourdomain.tld, codius02.dev.yourdomain.tld.
hostname "#{node['codius']['name']['host']}.#{node['codius']['name']['domain']}" do
    action :set
end

# Install node source
remote_file "#{node['temp_dir']}/#{node['moneyd']['nodejs']['local_script_name']}" do
    source "#{node['moneyd']['nodejs']['installer_script_source']}"
    owner 'root'
    group 'root'
    mode '0700'
    action :create
end

# Use the nodejs installer script to build and deploy a nodejs rpm.
execute 'build_nodejs' do
    cwd "#{node['temp_dir']}"
    action :run
    command "bash #{node['temp_dir']}/#{node['moneyd']['nodejs']['local_script_name']}" 
end

# Install dependencies (gcc-c++, make, git, etc...).
node['codius']['packages']['dependencies'].each do |p|
    package p do 
        action :install
    end
end

# Download Hyperd build/installation script.
remote_file "#{node['temp_dir']}/#{node['hyperd']['local_script_name']}" do
    source "#{node['hyperd']['installer_script_source']}"
    owner 'root'
    group 'root'
    mode '0700'
    action :create
end

# Att the time of this code's creation this script installs and starts hyperd 
# but doesn't enable it for reboots. Default vendor preset is disabled.
execute 'build_hyperd' do
    cwd "#{node['temp_dir']}"
    action :run
    command "bash #{node['temp_dir']}/#{node['hyperd']['local_script_name']}" 
end

# This ensures it will startup on reboots.
service 'hyperd' do
    action :enable
end

# Downlaod the moneyd rpm but (for now) this is always started manually 
# because it requires an XRP wallet secret key (and we don't want to store that anywhere).
# TODO: Handle moneyd testnet by default.
remote_file "#{node['temp_dir']}/#{node['moneyd']['local_rpm_name']}" do 
    source "#{node['moneyd']['rpm_uri']}"
    owner 'root'
    group 'root'
    mode '0700'
    action :create
end

# Install the rpm.
package "#{node['moneyd']['local_rpm_name']}" do
    source "#{node['temp_dir']}/#{node['moneyd']['local_rpm_name']}"
    action :install
end

# Disable the moneyd service.
service "#{node['moneyd']['service_name']}" do
    action :enable
end

# Install codiusd node module using npm.
execute 'install_codius_node_module' do
    command "npm install -g #{node['codius']['name']['daemon']} --unsafe-perm"
    action :run
end

# Deploy codiusd systemd configuration.
template "/etc/systemd/system/#{node['codius']['name']['daemon']}.service" do
    source 'codiusd.service'
    variables(
        'host_fqdn': "#{node['codius']['name']['host']}.#{node['codius']['name']['domain']}"
    )
end

# Disable codiusd
service "#{node['codius']['name']['daemon']}" do
    action :disable
end


# Get domain information from AWS and configure (keep in mind that DNS work was already done by cloudformation).
# Setup certbot to get SSL certs that will be rotated automatically.
# Setup SSL certs.

# Install and configure nginx.

# If production - Remove dependencies that are not production.
