# Using this cookbook to setup a Codius Host

## Before you begin:

If you wish to use this cookbook to deploy to AWS follow these steps.
1. Install the [latest Chef Dev Kit](https://downloads.chef.io/chefdk/).
1. Test that you've installed chef correctly.
   ```
   chef --version
   
   # If you installed it correctly you should see.
   Chef Development Kit Version: 3.0.36
   chef-client version: 14.1.12
   delivery version: master (7206afaf4cf29a17d2144bb39c55b7212cfafcc7)
   berks version: 7.0.2
   kitchen version: 1.21.2
   inspec version: 2.1.72
   ```
1. Install the [latest AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/installing.html).
1. Test that you installed the AWS CLI correctly.
   ```
   aws --version

   # If you did it correctly you should see something like this.
   aws-cli/1.15.35 Python/3.6.5 Darwin/17.6.0 botocore/1.10.35
   ```
1. [Configure your AWS credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html).
1. Test your ability to connect to AWS from this host.
   ```
   aws sts get-caller-identity
   
   # If everything is setup correctly you'll see something like this.
   {
       "UserId": "YOURIAMUSERIDENTITYID",
       "Account": "111111111111",
       "Arn": "arn:aws:iam::111111111111:user/youriamusername"
   }
   ```

If you wish to test locally do these also.
1. Install the [latest Vagrant](https://www.vagrantup.com/downloads.html).
1. Install the [latest VirtualBox](https://www.virtualbox.org/wiki/Downloads). 

## Validate your AWS infrastructure is setup to host your Codius Service:

***Note:*** Doing this will start an EC2 instance in AWS (which you'll be charged for if you're not eligible for the free tier.)

1. Enter the cookbook directory.
    ```
    cd cookbook
    ```
1. Create an environment file that has your environment variables in it.
    ```
    #!/usr/bin/env bash
    # Contents of ~/.codius-service-profile

    # Required environment variables.
    export AWS_HOST_KEY_NAME=<name-of-public-key-pair-you-previously-setup-in-aws> # Update this with your key name.
    export LOCAL_SSH_KEY=~/.ssh/id_rsa
    export IMAGE=ami-d5bf2caa # This value is the current version 7 image from CentOS as of June 14, 2018.
    export HOST_FQDN=dev.codius.yourDomain.tld
    export ENVIRONMENT=dev

    # Optional tags and their default values in .kitchen.yml.
    export TEST_KITCHEN_AZ=us-west-2a
    export TEST_KITCHEN_INSTANCE_TYPE=t2.micro
    export TEST_KITCHEN_REGION=us-east-1
    export TEST_KITCHEN_SG=codius-ingress-sg
    export TEST_KITCHEN_SUBNET=codius-ingress-subnet
    ```
1. Source the profile.
    ```
    source ~/.test-kitchen-profile
    ```
1. Create a symlink to the AWS kitchen config.
    ```
    ln -s .kitchen.aws.yml .kitchen.yml
    ```
1. Run test kitchen.
    ```
    kitchen test
    ```

Running `kitchen test` will do the following things.
* Start up an EC2 instance in AWS.
* Install Codius and its dependencies.
* Test the configuration.
* Tear down the EC2 instance.

If all those things don't happen it's likely that your AWS infrastructure configuration needs to be changed.

## Deploy to AWS:

***Note*** Test kitchen isn't strictly meant for deploying a production grade host, 
but for the time being it can "stand in" for proper deployment infrastructure to 
deploy a Codius Host while the ChainSmarts app is being developed.

1. Perform all the steps in the "Before you begin" section above.
1. Perform all the steps in the "Validate your AWS infrastructure is setup to host your Codius Service" section above.
1. Enter the cookbook directory.
    ```
    cd cookbook
    ```
1. Construct the host.
    ```
    kitchen create
    ```
1. Install everything on the host.
    ```
    kitchen converge
    ```
1. Validate the installation went as expected.
    ```
    kitchen test
    ```
## Post deployment configuration:

These are items that are outside of the scope of a cookbook to perform.
The ChainSmarts app (currently in the design stages) will do this for you when it's done.

1. Setup an EIP and bind it to your EC2 instance.
1. Update your DNS to point to your new host's public IP.
1. Log into the server.
   1. Get an SSL certificate from 
   1. Startup moneyd-xrp (with your xrp secrect key).
   1. Startup codiusd.
   1. Startup nginx. 
1. Connect to your server and check its codius version.
    ```
    # Run this command (replacing the domain and hostname with your own).
    curl https://yourCodiusHost.yourDomain.tld/version
    
    # Should return a response that looks like this.
    {
        "name": "Codiusd (JavaScript)",
        "version": "1.0.3"
    }
    ```
