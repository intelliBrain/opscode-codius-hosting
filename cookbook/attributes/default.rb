# TODO: Handle multiple hosts in the future.
default['codius']['name']['host'] = 'dev.codius'
default['codius']['name']['domain'] = 'chainsmarts.io'
default['codius']['name']['daemon'] = 'codiusd'

# Globally used attributes.
default['temp_dir'] = '/tmp'

# Dependencies from the *nix community.
default['codius']['packages']['dependencies'] = [
    'epel-release',
    'gcc-c++',
    'git',
    'make',
    'nginx',
    'nodejs'
]

# Moneyd dependencies.
default['moneyd']['nodejs']['installer_script_source'] = 'https://rpm.nodesource.com/setup_10.x'
default['moneyd']['nodejs']['local_script_name'] = 'nodejs-setup_10.x.sh'
default['moneyd']['rpm_uri'] = 'https://s3.us-east-2.amazonaws.com/codius-bucket/moneyd-xrp-4.0.0-1.x86_64.rpm'
default['moneyd']['local_rpm_name'] = 'moneyd-xrp-4.0.0-1.x86_64.rpm'
default['moneyd']['service_name'] = 'moneyd-xrp'

# Hyperd installer details.
default['hyperd']['installer_script_source'] = 'https://coiltest.s3.amazonaws.com/upload/latest/hyper-bootstrap.sh'
default['hyperd']['local_script_name'] = 'hyperd-bootstrap.sh'
